<?php

namespace App\Controller;

use App\Entity\Score;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiScoreController extends AbstractController
{
    #[Route('/api/leaderboard', name: 'api_leaderboard', methods:"GET")]
    public function index(ScoreRepository $scoreRepository, SerializerInterface $serializer)
    {
        // TUTO ICI -----> https://www.youtube.com/watch?v=SG7GgcnR1F4

        // $scores = $scoreRepository->findAll();
        
        // $postsNormalises = $normalizer->normalize($scores, null, ['groups' => 'post:read']);

        // $json =json_encode($postsNormalises);

        // $json = $serializer->serialize($scores, 'json', ['groups' => 'post:read']);

        // $response = new JsonResponse($json, 200, [], true);

        // dd($json, $scores);
        // return $this->render('api/index.html.twig', [
        //     'controller_name' => 'ApiController',
        // ]);
        // return $response;

        return $this->json($scoreRepository->findAll(), 200, [], ['groups' => 'post:read']);
    }

    #[Route('/api/top3', name: 'api_top3', methods:"GET")]
    public function top3(ScoreRepository $scoreRepository, SerializerInterface $serializer)
    {
        return $this->json($scoreRepository->returntop3(), 200, [], ['groups' => 'post:read']);
    }

    #[Route('/api/top10', name: 'api_top10', methods:"GET")]
    public function top10(ScoreRepository $scoreRepository, SerializerInterface $serializer)
    {
        return $this->json($scoreRepository->returntop10(), 200, [], ['groups' => 'post:read']);
    }

    #[Route('/api/leaderboard', name: 'api_leaderboard_create', methods:"POST")]
    public function create(Request $request, SerializerInterface $serializer, EntityManagerInterface $em) {
        $jsonRecu = $request->getContent();

        $scores = $serializer->deserialize($jsonRecu, Score::class, 'json');

        $scores->setCreatedAt(new \DateTimeImmutable());
 
        $em->persist($scores);
        $em->flush();

        dd($scores);

    }
}
