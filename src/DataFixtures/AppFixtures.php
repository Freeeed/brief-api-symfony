<?php

namespace App\DataFixtures;

use App\Entity\Score;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $date= new \DateTimeImmutable('NOW');
        // $product = new Product();
        // $manager->persist($product);
        for ($i = 0; $i < 10; $i++) {
            $score = new Score();
            $score->setPseudo('pseudo '.$i);
            $score->setScore(mt_rand(10, 100));
            $score->setCreatedAt($date);
            $manager->persist($score);

        }
        $manager->flush();
}
}